# How to use

## Required modules 

* pyqt5
* astropy
* numpy

## Prepare

Add the PYTHONPATH to use triccsred module

Example:  
Add the following sentence in ~/.bash_aliases. 
Fill the proper path in (triccsred path).

* export PYTHONPATH="(triccsred path):$PYTHONPATH"

## Usage:

python3 -m triccsred
