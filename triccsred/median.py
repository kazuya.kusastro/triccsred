#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### import libraries
import argparse
import os
import sys

from astropy.io import fits
import numpy as np

# original module
import data_func

### define functions
def main(input_name, sub_name, scale=1, keep3d=False):
    data, hdr = fits.getdata(input_name, view=np.ndarray, header=True)
    data_sub = fits.getdata(sub_name, view=np.ndarray)

    data, hdr = data_func.debias(data, hdr)
    data = data_func.subtract(data, data_sub * float(scale))

    if not keep3d:
        data = data_func.median(data)
#    data_2d = data_2d - data_sub * scale

    output_name = input_name.replace('.fits', '-median.fits')
    fits.writeto(output_name, data, overwrite=True)

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='debias, combine, and subtract TriCCS frame')

    parser.add_argument('input_name', help='input fits file')
    parser.add_argument('sub_name', help='subtract fits file')
    parser.add_argument('--scale', default=1, help='scale in subtracton')
    parser.add_argument('--keep3d', action='store_true', help='keep 3D fits format')

    args = parser.parse_args()

    main(args.input_name, args.sub_name, scale=args.scale, keep3d=args.keep3d)

