#!/usr/bin/env python3

### main_window.py: the main window of triccsred

### import libraries
import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow, QTabWidget, QMessageBox, QGridLayout)
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

# TriCCS reduction modules
from triccsred import gui 
from triccsred.const_var import const_var

## define main window class
class MainWindow(QWidget):
# define constants

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

# initialize
        self.tab_list = (
            {'key': 'setting', 'tab_name': 'Settings', 'inst': gui.setting.Frame(self)},
            {'key': 'input', 'tab_name': 'Input files', 'inst': gui.input_frame.Frame(self)},
            {'key': 'dark_flat', 'tab_name': 'Dark/Flat', 'inst': gui.dark_flat.Frame(self)},
            {'key': 'offset_combine', 'tab_name': 'Offset/Combine', 'inst': gui.offset_combine.Frame(self)}
        )

        self.frame = {}
        self.old_tab_index = 0

# define labels, buttons, ...
        self.tabs = QTabWidget()
        for tmp_frame in self.tab_list:
            self.frame[tmp_frame['key']] = tmp_frame['inst']
            self.tabs.addTab(self.frame[tmp_frame['key']], tmp_frame['tab_name'])

#    self.frameOverscanBias = koolsred.GUI.OverscanBias.boxOverscanBias(self)
#    self.frameCombFlatComp = koolsred.GUI.CombFlatComp.boxCombineFlat(self)
#        self.frameBiasFlat = koolsred.gui.bias_flat.boxBiasFlat(self)
#    self.frameNoGrismImage = koolsred.GUI.NoGrismImage.boxNoGrismImage(self)
#    self.frameQuickImage = koolsred.GUI.QuickImage.boxQuickImage(self)
#        self.frameExtractSpec = koolsred.gui.extract_spec.boxExtractSpec(self)
#        self.frameWaveCalib = koolsred.gui.wave_calib.boxWaveCalib(self)
#        self.frame_sky_subtract = koolsred.gui.sky_subtract.Frame(self)

#        self.tabs.addTab(self.frame['setting'], 'Settings')
#        self.tabs.addTab(self.frame['input'], 'Input files')
#    tabs.addTab(self.frameOverscanBias, 'Overscan and bias')
#        tabs.addTab(self.frameBiasFlat, 'Bias and flat')
#    tabs.addTab(self.frameNoGrismImage, 'Make image (without grism)')
#    tabs.addTab(self.frameQuickImage, 'Make quick image (with grism)')
#        tabs.addTab(self.frameExtractSpec, 'Extract spectra')
#        tabs.addTab(self.frameWaveCalib, 'Wavelength calibration')
#        tabs.addTab(self.frame_sky_subtract, 'Sky subtraction')
#        tabs.setFont(koolsred.const_var.font)
        self.tabs.currentChanged.connect(self.change_tab)

        self.tabs.setFont(const_var['font'])

#        self.frameOpenFits = koolsred.gui.open_fits.boxOpenFits(self)

# layout
#    self.layout = QVBoxLayout()
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(self.tabs, 0, 0)
#        layout.addWidget(self.frameOpenFits, 1, 0)

        self.setLayout(layout)
        self.resize(800, 600)
        self.setWindowTitle('TriCCS data reduction GUI')

# function
    def change_tab(self):
#        print(self.old_tab_index, '->', self.tabs.currentIndex())
        self.tab_list[self.old_tab_index]['inst'].update_info()
        self.old_tab_index = self.tabs.currentIndex()

        return

#        self.frame_setting.update_gui()
#        self.frameInput.updateGUI()
#    self.frameNoGrismImage.updateGUI()
#    self.frameQuickImage.updateGUI()
#        self.frameExtractSpec.updateGUI()
#        self.frameWaveCalib.updateGUI()
#        self.frame_sky_subtract.update_gui()

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message', 'Are you sure to close this GUI?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
#    reply.setFont(const_var.fontLabel)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

        return

### main function
def show(args):
# start GUI
    app = QApplication(sys.argv)
    main_window = MainWindow()

# show main window
    main_window.show()
# exit main function if exit called
    sys.exit(app.exec_())

    return
