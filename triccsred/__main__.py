#!/usr/bin/env python3

### __main__.py: the main program of triccsred

### import libraries
import argparse

# TriCCS reduction modules
from triccsred import main_window

def main(args):
    main_window.show(args)

    return

if __name__ == '__main__':
# argparse
    parser = argparse.ArgumentParser(description='TriCCS data reduction GUI')
#    parser.add_argument('-t', '--test', action='store_true', help='Test.')

    main(parser.parse_args())

