#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### import libraries
import argparse
import os
import sys

from astropy.io import fits
import numpy as np

# original module
import data_func

### define functions
def main(input_name):
    data, hdr = fits.getdata(input_name, view=np.ndarray, header=True)

    data = data_func.debias(data)

    output_name = input_name.replace('.fits', '-debias.fits')
    fits.writeto(output_name, data, hdr, overwrite=True)

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='debias TriCCS frame')

    parser.add_argument('input_name', help='input fits file')

    args = parser.parse_args()

    main(args.input_name)

