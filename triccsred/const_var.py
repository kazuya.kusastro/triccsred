#!/usr/bin/env python3

### const_var.py: define constants and variables

### import libraries
from PyQt5.QtGui import QFont

### set constants
const_var = {
# constants
    'fits_name_format_choices': ('TRCS%08d.fits', ),
    'filter_grism_list': ('None', 'PS1-g', 'PS1-r', 'PS1-i', 'PS1-z'),

# font
    'font_large': QFont(),
    'font': QFont(),
    'font_tooltip': QFont(),

### define variables
    'processing': None,

    'fits_name_format': 'TRCS%08d.fits',
    'fits_list': []
}

## additional processes
# font
const_var['font_large'].setPixelSize(28)
const_var['font'].setPixelSize(20)
const_var['font_tooltip'].setPixelSize(16)
