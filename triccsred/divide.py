#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### import libraries
import argparse
import os
import sys

from astropy.io import fits
import numpy as np

# original module
import data_func

### define functions
def main(input_name, div_name, output_name):
    data, hdr = fits.getdata(input_name, view=np.ndarray, header=True)
    data_div = fits.getdata(div_name, view=np.ndarray)

    data = data_func.div_flat(data, data_div)

    fits.writeto(output_name, data, hdr, overwrite=True)

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='subtract TriCCS frame keeping 3D format')

    parser.add_argument('input_name', help='input fits file')
    parser.add_argument('div_name', help='divide fits file')
    parser.add_argument('output_name', help='output fits file')

    args = parser.parse_args()

    main(args.input_name, args.div_name, args.output_name)

