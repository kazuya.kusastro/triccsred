#!/usr/bin/env python3

### gui/frame_input.py: GUI for the Input window

### import modules
import glob
import os
import pickle

from astropy.io import fits

import PyQt5
from PyQt5.QtWidgets import (QLabel, QLineEdit, QTableWidget, QTableWidgetItem, QPushButton, QComboBox, QRadioButton, QButtonGroup, QFileDialog, QFrame, QGridLayout)
from PyQt5.QtCore import Qt

# original modules
from triccsred.const_var import const_var

### set constants

### define classes
class Frame(QFrame):
# define constants
    HEADER_TABLE = ('Frame ID', 'Data type', 'Exp. time', 'num. of frame', 'Gain', 'Filter/Grism')
    FITS_PICKLE = 'fits_list.pl'

    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.fits_list = []

#        self.label = {}
#        self.inputbox = {}
#        self.combobox = {}
        self.button = {}
#        self.radio = {}
#        self.group = {}

# make buttons and input/output boxes
        self.button['import_fits'] = QPushButton('Import from fits')
        self.button['import_fits'].clicked.connect(self.import_fits)
        self.button['import_fits'].setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.button['import_pickle'] = QPushButton('Import from pickle')
        self.button['import_pickle'].clicked.connect(self.import_pickle)
        self.button['import_pickle'].setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.button['export_pickle'] = QPushButton('Export to pickle')
        self.button['export_pickle'].clicked.connect(self.export_pickle)
        self.button['export_pickle'].setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.table_frame = QTableWidget(0, 6)
        self.table_frame.setHorizontalHeaderLabels(Frame.HEADER_TABLE)
        self.table_frame.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')
        self.table_frame.setColumnWidth(1, 120)

#        self.label['name_format'] = QLabel('Frame name format')

#        self.combobox['name_format'] = QComboBox()
#        [self.combobox['name_format'].addItem(i) for i in const_var['name_format']]

# set the common settings
#        [self.label[i].setFont(const_var['font']) for i in self.label]
#        [self.combobox[i].setFont(const_var['font']) for i in self.label]
        [self.button[i].setFont(const_var['font']) for i in self.button]
#        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
#        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
#        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]
#        [self.radio[i].setFont(const_var['font']) for i in self.radio]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(self.button['import_fits'], count_row, 0)
        layout.addWidget(self.button['import_pickle'], count_row, 1)
        layout.addWidget(self.button['export_pickle'], count_row, 2)
        count_row += 1

        layout.addWidget(self.table_frame, count_row, 0, 1, 3)

        self.setLayout(layout)

### define functions
    def import_fits(self):
        fits_name = glob.glob(self.fits_name_format(const_var['fits_name_format']))

        self.fits_list = []
        for tmp_fits in fits_name:
            tmp_param = {}

            hdr = fits.getheader(tmp_fits)
            tmp_param['frame_id'] = int(str(hdr['EXPID']) + str(hdr['DETID']))
            tmp_param['data_type'] = hdr['DATA-TYP']
            tmp_param['exp_time'] = hdr['EXPTIME1']
            tmp_param['n_frame'] = hdr['NAXIS3']
            tmp_param['gain'] = hdr['GAINCNFG']
            tmp_param['filter_grism'] = hdr['FILTDISP']

            self.fits_list.append(tmp_param)

        self.fits_list.sort(key=lambda x: x['frame_id'])
        self.make_frame_table()

        return

    @staticmethod
    def fits_name_format(name_format):
        if name_format == 'TRCS%08d.fits':
            return './TRCS[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-2].fits'
        else:
            return './*.fits'

    def import_pickle(self):
        if not os.path.exists(Frame.FITS_PICKLE):
            print(Frame.FITS_PICKLE, ' does not exist. Quit import.')
            return

        with open(Frame.FITS_PICKLE, 'rb') as fin:
            self.fits_list = pickle.load(fin)

        self.make_frame_table()

        return

    def export_pickle(self):
        if len(self.fits_list) == 0:
            print('Frame list is None. Do not output file.')
            return

        with open(Frame.FITS_PICKLE, 'wb') as fout:
            pickle.dump(self.fits_list, fout, pickle.HIGHEST_PROTOCOL)
            print('Export a fits list:', Frame.FITS_PICKLE)

        return

    def make_frame_table(self):
        for row_number, tmp_frame in enumerate(self.fits_list):
            self.table_frame.insertRow(row_number)

            item = QTableWidgetItem(str(tmp_frame['frame_id']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 0, item)

            item = QTableWidgetItem(str(tmp_frame['data_type']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 1, item)

            item = QTableWidgetItem(str(tmp_frame['exp_time']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 2, item)

            item = QTableWidgetItem(str(tmp_frame['n_frame']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 3, item)

            item = QTableWidgetItem(str(tmp_frame['gain']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 4, item)

            item = QTableWidgetItem(str(tmp_frame['filter_grism']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 5, item)


    def update_info(self):
        const_var['fits_list'] = self.fits_list

        return

