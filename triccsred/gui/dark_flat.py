#!/usr/bin/env python3

### gui/dark_flat.py: GUI for the Dark/Flat window

### import modules
import glob
import os

import PyQt5
from PyQt5.QtWidgets import (QLabel, QLineEdit, QTableWidget, QTableWidgetItem, QCheckBox, QPushButton, QComboBox, QRadioButton, QButtonGroup, QFileDialog, QFrame, QGridLayout)
from PyQt5.QtCore import Qt

# original modules
from triccsred.const_var import const_var
from triccsred.data_red import dark_flat
from triccsred.gui import gui_func

### define constants

### define classes
class Frame(QFrame):
# define class constants
    HEADER_TABLE = ('to be combined', 'Frame ID', 'Data type', 'Exp. time', 'num. of frame', 'Gain')

    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.label = {}
        self.combobox = {}
#        self.inputbox = {}
        self.button = {}
        self.radio = {}

# make buttons and input/output boxes
        self.label_dark = QLabel('Dark')
        self.label_dark.setFont(const_var['font_large'])

        self.label['debias'] = QLabel('Debias')
        self.radio['debias_yes'] = QRadioButton('Yes')
        self.radio['debias_yes'].setChecked(False)
        self.radio['debias_no'] = QRadioButton('No')
        self.radio['debias_no'].setChecked(True)
        self.group_debias = QButtonGroup()
        self.group_debias.addButton(self.radio['debias_yes'])
        self.group_debias.addButton(self.radio['debias_no'])

        self.label['combine'] = QLabel('Combine')
        self.radio['combine_mean'] = QRadioButton('Mean')
        self.radio['combine_mean'].setChecked(False)
        self.radio['combine_median'] = QRadioButton('Median')
        self.radio['combine_median'].setChecked(True)
        self.group_combine = QButtonGroup()
        self.group_combine.addButton(self.radio['combine_mean'])
        self.group_combine.addButton(self.radio['combine_median'])

        self.button['make_dark'] = QPushButton('Make dark frames')
        self.button['make_dark'].clicked.connect(lambda: gui_func.check_process(self.make_dark_frames))
        self.button['make_dark'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['subtract_dark'] = QPushButton('Subtract dark frames')
        self.button['subtract_dark'].clicked.connect(lambda: gui_func.check_process(self.subtract_dark))
        self.button['subtract_dark'].setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.label['grid_line1'] = QLabel('-' * 30)
        self.label['grid_line1'].setAlignment(Qt.AlignCenter)

        self.label_flat = QLabel('Flat')
        self.label_flat.setFont(const_var['font_large'])

        self.label['filter_grism'] = QLabel('Filter/Grism')
        self.combobox['filter_grism'] = QComboBox()
        [self.combobox['filter_grism'].addItem(i) for i in ('----', 'PS1-g', 'PS1-r', 'PS1-i', 'PS1-z')]
        self.combobox['filter_grism'].currentIndexChanged.connect(self.update_filter_grism)

        self.table_frame = QTableWidget(0, 6)
        self.table_frame.setHorizontalHeaderLabels(Frame.HEADER_TABLE)
        self.table_frame.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')
        self.table_frame.setColumnWidth(1, 120)

        self.button['make_flat'] = QPushButton('Make a flat frame')
        self.button['make_flat'].clicked.connect(lambda: gui_func.check_process(self.make_flat_frame))
        self.button['make_flat'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['flat_field'] = QPushButton('Flat fielding')
        self.button['flat_field'].clicked.connect(lambda: gui_func.check_process(self.flat_fielding))
        self.button['flat_field'].setStyleSheet('QWidget { background-color: #B0FFB0 }')


# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
#        [self.combobox[i].setFont(const_var['font']) for i in self.label]
        [self.button[i].setFont(const_var['font']) for i in self.button]
#        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
#        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
#        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]
        [self.radio[i].setFont(const_var['font']) for i in self.radio]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(self.label_dark, count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.label['debias'], count_row, 0)
        layout.addWidget(self.radio['debias_yes'], count_row, 1)
        layout.addWidget(self.radio['debias_no'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['combine'], count_row, 0)
        layout.addWidget(self.radio['combine_mean'], count_row, 1)
        layout.addWidget(self.radio['combine_median'], count_row, 2)
        count_row += 1

        layout.addWidget(self.button['make_dark'], count_row, 0)
        layout.addWidget(self.button['subtract_dark'], count_row, 1, 1, 2)
        count_row += 1

        layout.addWidget(self.label['grid_line1'], count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.label_flat, count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.label['filter_grism'], count_row, 0)
        layout.addWidget(self.combobox['filter_grism'], count_row, 1)
        count_row += 1

        layout.addWidget(self.table_frame, count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.button['make_flat'], count_row, 0)
        layout.addWidget(self.button['flat_field'], count_row, 1, 1, 2)

        self.setLayout(layout)

        return

### define functions
    def make_dark_frames(self):
        print('Dark frame creation starts.')

# collect dark frames with the same parameters
        dark_dict = {}
        for tmp_fits in const_var['fits_list']:
            if tmp_fits['data_type'] != 'DARK':
                continue

            frame_id, arm, exp_time, gain = self.convert_params(tmp_fits)
            if (arm, exp_time, gain) in dark_dict:
                dark_dict[(arm, exp_time, gain)].append(frame_id)
            else:
                dark_dict[(arm, exp_time, gain)] = [frame_id]

# combine dark frames
        dark_keys = dark_dict.keys()
        for tmp_key in dark_keys:
            input_frames = ['TRCS{}.fits'.format(str(i).zfill(8)) for i in dark_dict[tmp_key]]
            output_name = 'lib/dark-arm{}-{}ms-gain{}.fits'.format(*tmp_key)
            debias = True if self.group_debias.checkedButton().text() == 'Yes' else False
            combine = self.group_combine.checkedButton().text().lower()

            print(','.join(input_frames), '->', output_name)

            dark_flat.make_dark(input_frames, output_name, debias=debias, combine=combine)

        print('Dark frame creation finished.')

        return

    def subtract_dark(self):
        print('Dark subtraction starts.')

# subtract dark if the proper dark frame exists
        for tmp_fits in const_var['fits_list']:
            if tmp_fits['data_type'] not in ('FLAT', 'OBJECT'):
                continue

            frame_id, arm, exp_time, gain = self.convert_params(tmp_fits)
            dark_name = 'lib/dark-arm{}-{}ms-gain{}.fits'.format(arm, exp_time, gain)
            if os.path.exists(dark_name):
                input_name = 'TRCS{}.fits'.format(str(frame_id).zfill(8))
                output_name = input_name.replace('.fits', '-dark.fits')
                print(input_name, '-', dark_name, '->', output_name)
                dark_flat.subtract_dark(input_name, output_name, dark_name)

        print('Dark subtraction finished.')

        return

    @staticmethod
    def convert_params(input_dict):
        frame_id = input_dict['frame_id']
        arm = chr(int(str(frame_id)[-1]) + 65)
        exp_time = str(int(input_dict['exp_time'] * 1000))
        gain = input_dict['gain'].lstrip('x')

        return (frame_id, arm, exp_time, gain)

    def update_filter_grism(self):
        self.table_frame.setRowCount(0)

        row_number = 0
        for tmp_frame in const_var['fits_list']:
            if tmp_frame['data_type'] == 'DARK' \
                or tmp_frame['filter_grism'] != self.combobox['filter_grism'].currentText() \
                or not os.path.exists('TRCS{}-dark.fits'.format(str(tmp_frame['frame_id']).zfill(8))):
                continue

            self.table_frame.insertRow(row_number)

            checkBox = QCheckBox()
            self.table_frame.setCellWidget(row_number, 0, checkBox)

            item = QTableWidgetItem(str(tmp_frame['frame_id']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 1, item)

            item = QTableWidgetItem(str(tmp_frame['data_type']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 2, item)

            item = QTableWidgetItem(str(tmp_frame['exp_time']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 3, item)

            item = QTableWidgetItem(str(tmp_frame['n_frame']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 4, item)

            item = QTableWidgetItem(str(tmp_frame['gain']))
            item.setFlags(Qt.ItemIsEnabled)
            self.table_frame.setItem(row_number, 5, item)

            row_number += 1

        return

    def make_flat_frame(self):
        print('Flat frame creation starts.')

        tmp_table = self.table_frame
        input_names = ['TRCS{}-dark.fits'.format(tmp_table.item(i, 1).text().zfill(8)) for i in range(tmp_table.rowCount()) if tmp_table.cellWidget(i, 0).isChecked()]
        output_name = 'lib/flat-{}.fits'.format(self.combobox['filter_grism'].currentText())
        combine = 'median'

        print(','.join(input_names), '->', output_name)
        dark_flat.make_flat(input_names, output_name, combine=combine)

        print('Flat frame creation finished.')

        return

    def flat_fielding(self):
        print('Flat fielding starts.')

# perform flat field dark if the proper frame frame exists
        for tmp_fits in const_var['fits_list']:
            if tmp_fits['data_type'] not in ('FLAT', 'OBJECT'):
                continue

            input_name = 'TRCS{}-dark.fits'.format(str(tmp_fits['frame_id']).zfill(8))
            if not os.path.exists(input_name):
                continue

            flat_name = 'lib/flat-{}.fits'.format(tmp_fits['filter_grism'])
            if not os.path.exists(flat_name):
                continue

            output_name = input_name.replace('-dark.fits', '-flat.fits')
            print(input_name, '/', flat_name, '->', output_name)
            dark_flat.flat_field(input_name, output_name, flat_name)

        print('Flat fielding finished.')

        return

    def update_info(self):
        pass

