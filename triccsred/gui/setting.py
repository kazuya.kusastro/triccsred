#!/usr/bin/env python3

### gui/setting.py: GUI for the Setting window

### import modules
import PyQt5
from PyQt5.QtWidgets import (QLabel, QLineEdit, QTableWidget, QPushButton, QComboBox, QRadioButton, QButtonGroup, QFileDialog, QFrame, QGridLayout)
from PyQt5.QtCore import Qt

# original modules
from triccsred.const_var import const_var

### set constants

### define classes
class Frame(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.label = {}
        self.combobox = {}
#        self.inputbox = {}
#        self.button = {}
#        self.radio = {}
#        self.group = {}

# make buttons and input/output boxes
        self.label['name_format'] = QLabel('Fits name format')

        self.combobox['name_format'] = QComboBox()
        [self.combobox['name_format'].addItem(i) for i in const_var['fits_name_format_choices']]
        self.combobox['name_format'].currentIndexChanged.connect(self.update_name_format)

# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
        [self.combobox[i].setFont(const_var['font']) for i in self.label]
#        [self.button[i].setFont(const_var['font']) for i in self.button]
#        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
#        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
#        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]
#        [self.radio[i].setFont(const_var['font']) for i in self.radio]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(self.label['name_format'], count_row, 0)
        layout.addWidget(self.combobox['name_format'], count_row, 1)
#        count_row += 1

        self.setLayout(layout)

### define functions
    def update_name_format(self):
        const_var['fits_name_format'] = self.combobox['name_format'].currentText()
        return

    def update_info(self):
#        print('test: setting')
        pass

