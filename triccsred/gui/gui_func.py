#!/usr/bin/ent python3

### gui/gui_func.py: define common functions for GUI

### import modules
import multiprocessing

# original modules
from triccsred.const_var import const_var

### define functions
def check_process(func):
    if const_var['processing'] != None and const_var['processing'].exitcode == None:
        print('Another process is running. Wait for a while.')
        return

    const_var['processing'] = multiprocessing.Process(target=func)
    const_var['processing'].start()

    return

