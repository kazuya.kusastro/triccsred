#!/usr/bin/env python3

### gui/offset_combine.py: GUI for the Offset/Combine window

### import modules
import glob
import os

import PyQt5
from PyQt5.QtWidgets import (QLabel, QLineEdit, QTableWidget, QTableWidgetItem, QCheckBox, QPushButton, QComboBox, QRadioButton, QButtonGroup, QFileDialog, QFrame, QGridLayout)
from PyQt5.QtCore import Qt

# original modules
from triccsred.const_var import const_var
from triccsred.data_red import offset_combine
from triccsred.gui import gui_func

### define constants

### define classes
class Frame(QFrame):
# define class constants
    HEADER_TABLE = ('to be combined', 'Frame ID', 'Object', 'Data type', 'Exp. time', 'num. of frame')

    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.label = {}
        self.combobox = {}
#        self.inputbox = {}
        self.button = {}
        self.radio = {}

# make buttons and input/output boxes
#        self.label_offset = QLabel('Offset')
#        self.label_offset.setFont(const_var['font_large'])

        self.label_combine = QLabel('Combine')
        self.label_combine.setFont(const_var['font_large'])

        self.label['combine'] = QLabel('Combine')
        self.radio['combine_mean'] = QRadioButton('Mean')
        self.radio['combine_mean'].setChecked(False)
        self.radio['combine_median'] = QRadioButton('Median')
        self.radio['combine_median'].setChecked(True)
        self.group_combine = QButtonGroup()
        self.group_combine.addButton(self.radio['combine_mean'])
        self.group_combine.addButton(self.radio['combine_median'])

#        self.table_frame = QTableWidget(0, 6)
#        self.table_frame.setHorizontalHeaderLabels(Frame.HEADER_TABLE)
#        self.table_frame.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')
#        self.table_frame.setColumnWidth(1, 120)

        self.button['combine'] = QPushButton('Combine frames')
        self.button['combine'].clicked.connect(lambda: gui_func.check_process(self.combine_frames))
        self.button['combine'].setStyleSheet('QWidget { background-color: #B0FFB0 }')

#        self.label['grid_line1'] = QLabel('-' * 30)
#        self.label['grid_line1'].setAlignment(Qt.AlignCenter)


# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
#        [self.combobox[i].setFont(const_var['font']) for i in self.label]
        [self.button[i].setFont(const_var['font']) for i in self.button]
#        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
#        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
#        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]
        [self.radio[i].setFont(const_var['font']) for i in self.radio]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(self.label_combine, count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.label['combine'], count_row, 0)
        layout.addWidget(self.radio['combine_mean'], count_row, 1)
        layout.addWidget(self.radio['combine_median'], count_row, 2)
        count_row += 1

#        layout.addWidget(self.table_frame, count_row, 0, 1, 3)
#        count_row += 1

        layout.addWidget(self.button['combine'], count_row, 0)
        count_row += 1

#        layout.addWidget(self.label['grid_line1'], count_row, 0, 1, 3)
#        count_row += 1

        self.setLayout(layout)

        return

### define functions
    def combine_frames(self):
        print('Frame combining starts.')

# subtract dark if the proper dark frame exists
        for tmp_fits in const_var['fits_list']:
            if tmp_fits['data_type'] not in ('FLAT', 'OBJECT') \
                or not os.path.exists('TRCS{}-flat.fits'.format(str(tmp_fits['frame_id']).zfill(8))):
                continue

            input_name = 'TRCS{}-flat.fits'.format(str(tmp_fits['frame_id']).zfill(8))
            output_name = input_name.replace('-flat.fits', '-comb.fits')
            combine = self.group_combine.checkedButton().text().lower()
            print(input_name, '->', output_name)
            offset_combine.combine_frames(input_name, output_name, combine)

        print('Dark subtraction finished.')

        return

    def update_info(self):
        pass

