#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### import libraries
import argparse
import gc
import os
import sys

from astropy.io import fits
import numpy as np

# original module
import data_func

### define constants
tmp_file_name = 'tmp_combine.dat'
limit_frame_default = 1000

### define functions
def process_normal(args):
    input_name, cut, debias = args.input_name, args.cut, args.no_debias

    print('input:', input_name)
    data = fits.getdata(input_name, view=np.ndarray)

    if debias:
        if cut is None:
            data = data_func.debias(data)
        else:
            data = data_func.debias(data[cut[0]:cut[1], :, :])

    if args.average:
        return data_func.combine_average(data)
    else:
        return data_func.combine_median(data)

#def process_large(input_name, hdr, step_z=limit_frame_default, cut=None):
def process_large(hdr, args):
    input_name, step_z, cut, debias = args.input_name, args.limit_frame, args.cut, args.no_debias

    size_data = (hdr['NAXIS3'], hdr['NAXIS2'], hdr['NAXIS1'])
    bzero = hdr.pop('BZERO')
    bscale = hdr.pop('BSCALE')

    data_mp = np.memmap(tmp_file_name, dtype='float32', mode='w+', shape=size_data)
    data_out = np.zeros((size_data[1:]))

    tmp_z = 0
    while tmp_z < size_data[0]:
        with fits.open(input_name, do_not_scale_image_data=True) as hdul:
            end_z = min(tmp_z + step_z, size_data[0])
            print('input: {} z = {}:{}'.format(input_name, tmp_z, end_z))
            data_mp[tmp_z:end_z, :, :] = hdul[0].data[tmp_z:end_z, :, :] * bscale + bzero
            tmp_z += step_z
            del hdul[0].data
            gc.collect()

    if debias:
        for count in range(size_data[0]):
            print('debias z =', count)
            data_mp[count, :, :] = data_func.debias(data_mp[count, :, :])
            if count // step_z == 0:
                gc.collect()

    for count in range(size_data[1]):
        print('combine y =', count)
        if args.average:
            data_out[count, :] = np.mean(data_mp[:, count, :], axis=0)
        else:
            data_out[count, :] = np.median(data_mp[:, count, :], axis=0)
        if count // step_z == 0:
            gc.collect()

    os.remove(tmp_file_name)

    return data_out

#def combine(input_name, limit_frame=limit_frame_default, cut=None, debias=True):
def combine(args):
    hdr = fits.getheader(args.input_name)
#   data, hdr = fits.getdata(input_name, view=np.ndarray, header=True)

    if hdr['NAXIS3'] <= args.limit_frame:
        try:
            data_2d = process_normal(args)
        except MemoryError:
            data_2d = process_large(hdr, args)
    else:
        data_2d = process_large(hdr, args)

    if not args.no_debias:
        hdr.add_history('debias')
    output_name = args.input_name.replace('.fits', '-combine.fits')
    fits.writeto(output_name, data_2d, hdr, overwrite=True)

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='debias and combine TriCCS frame')

    parser.add_argument('input_name', help='input fits file')
    parser.add_argument('-a', '--average', action='store_true', help='combine with average (default is median)')
    parser.add_argument('-l', '--limit_frame', type=int, default=limit_frame_default, help='limit in input frame number')
    parser.add_argument('-c', '--cut', nargs=2, type=int, help='frame numbers in combining')
    parser.add_argument('--no_debias', action='store_false', help='no debias process')

    args = parser.parse_args()
    combine(args)

#    if args.cut:
#        combine(args.input_name, args.limit_frame, args.cut)
#    else:
#        combine(args.input_name, args.limit_frame)

