#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### import libraries
import argparse
import os
import sys

from astropy.io import fits
import numpy as np

# original module
import data_func

### define constants

### define functions
def main(input_name, sub_name, output_name, scale=1):
    data, hdr = fits.getdata(input_name, view=np.ndarray, header=True)
    data_sub = fits.getdata(sub_name, view=np.ndarray)

    data = data_func.subtract(data, data_sub * float(scale))

    fits.writeto(output_name, data, hdr, overwrite=True)

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='subtract TriCCS frame keeping 3D format')

    parser.add_argument('input_name', help='input fits file')
    parser.add_argument('sub_name', help='subtract fits file')
    parser.add_argument('output_name', help='output fits file')
    parser.add_argument('--scale', default=1, help='scale in subtracton')

    args = parser.parse_args()

    main(args.input_name, args.sub_name, args.output_name, scale=args.scale)

