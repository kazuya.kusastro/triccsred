#!/usr/bin/env python3

### data_red/dark_flat.py: make dark and flat frames, and subtract/divide them.

### import libraries
import argparse
import os
import sys

from astropy.io import fits
import numpy as np

# original module

### define functions
def make_dark(input_names, output_name, debias=False, combine='median'):
# conjunction the input frames
    data_in, header_in = fits.getdata(input_names[0], view=np.ndarray, header=True)

    if len(input_names) > 1:
        for tmp_name in input_names[1:]:
            data_in = np.concatenate((data_in, fits.getdata(tmp_name, view=np.ndarray)), axis=0)

# do debias
    if debias:
        data_in = do_debias(data_in)

    header_in['R_DEBIAS'] = (debias, 'Debias to each frame')

# combine with mean or median
    if combine == 'mean':
        data_out = np.average(data_in, axis=0)
    else:
        data_out = np.median(data_in, axis=0)

    header_in['R_DARK_C'] = (combine, 'Function used in combining dark frames')

# add a comment on the fits header
    header_in.append(('COMMENT', 'Combined frames: ' + ','.join(input_names)), end=True)

# make the lib directory
    if not os.path.isdir('lib'):
        os.mkdir('lib')

# output the fits file
    fits.writeto(output_name, data_out, header_in, overwrite=True)

    return

def subtract_dark(input_name, output_name, dark_name=None):
# input parameters
    data_in, header_in = fits.getdata(input_name, view=np.ndarray, header=True)
    data_dark, header_dark = fits.getdata(dark_name, view=np.ndarray, header=True)

# do debias
    debias_dark = header_dark['R_DEBIAS']
    header_in['R_DEBIAS'] = (debias_dark, 'Debias to each frame')
    if debias_dark:
        data_in = do_debias(data_in)

# subtract dark
    data_in = subtract(data_in, data_dark)
    dark_name_cut = dark_name.split('/')[-1]
    header_in['R_DARK_F'] = (dark_name_cut, 'Fits used in dark subtraction')

# output the fits file
    fits.writeto(output_name, data_in, header_in, overwrite=True)

    return

def make_flat(input_names, output_name, combine='median'):
# define constant
    x1, x2, y1, y2 = 1000, 1100, 550, 650

# conjunction the input frames
    data_in, header_in = fits.getdata(input_names[0], view=np.ndarray, header=True)

    if len(input_names) > 1:
        for tmp_name in input_names[1:]:
            data_in = np.concatenate((data_in, fits.getdata(tmp_name, view=np.ndarray)), axis=0)

# normarize with the center count
    for count in range((data_in.shape)[0]):
        data_in[count, :, :] = data_in[count, :, :] / np.median(data_in[count, y1:y2, x1:x2])

# combine with mean or median
    if combine == 'mean':
        data_out = np.average(data_in, axis=0)
    else:
        data_out = np.median(data_in, axis=0)

    header_in['R_FLAT_C'] = (combine, 'Function used in combining flat frames')

# add a comment on the fits header
    header_in.append(('COMMENT', 'Combined frames: ' + ','.join(input_names)), end=True)

# make the lib directory
    if not os.path.isdir('lib'):
        os.mkdir('lib')

# output the fits file
    fits.writeto(output_name, data_out, header_in, overwrite=True)

    return

def flat_field(input_name, output_name, flat_name):
# input parameters
    data_in, header_in = fits.getdata(input_name, view=np.ndarray, header=True)
    data_flat = fits.getdata(flat_name, view=np.ndarray)

# flat fielding
    data_in = div_flat(data_in, data_flat)
    flat_name_cut = flat_name.split('/')[-1]
    header_in['R_FLAT_F'] = (flat_name_cut, 'Fits used in flat fielding')

# output the fits file
    fits.writeto(output_name, data_in, header_in, overwrite=True)

    return


def do_debias(data):
    if data.ndim == 2:
        y,x = data.shape
        z = 1
        data = data.reshape((z,y,x))
    elif data.ndim == 3:
        z,y,x = data.shape

    for n in range(z):
        d = np.zeros((18, 4, x))
        for count in range(18):
            d[count, :, :] = data[n, (count * 4 + 1284):(count * 4 + 1288), :]
        d = np.median(d, axis=0)
        d_tile = np.tile(d, (int(y / 4), 1))
        data[n, :, :] -= d_tile 

    if z==1:
        data = data.reshape((y,x))

    return data

def subtract(data, data_sub):
    if data.ndim == 2:
        return data - data_sub
    elif data.ndim == 3:
        z, y, x = data.shape
        for count in range(z):
            data[count, :, :] = data[count, :, :] - data_sub
        return data

#        if z <= limit_frame:
#            data_sub_3d = np.tile(data_sub, (z, 1, 1))
#            return data - data_sub_3d
#        else:
#            data_mp = np.memmap(tmp_file_name, dtype='float32', mode='w+', shape=(z, y, x))
#            for count in range(z):
#                print('subtract z =', count)
#                data_mp[count, :, :] = data[count, :, :] - data_sub
#            return data_mp
    else:
        return data

def div_flat(data, data_flat):
    if data.ndim == 2:
        return np.divide(data, data_flat, out=np.zeros_like(data), where=data_flat!=0)
    elif data.ndim == 3:
        z, y, x = data.shape
#        data_flat_3d = np.tile(data_flat, (z, 1, 1))
        for count in range(z):
            data[count, :, :] = np.divide(data[count, :, :], data_flat, where=data_flat!=0)
        return data
#        return np.divide(data, data_flat_3d, out=np.zeros_like(data), where=data_flat!=0)

    return


