#!/usr/bin/env python3

### data_red/offset_combine.py: offset among the same fits file and combine the frames

### import libraries
import argparse
import os
import sys

from astropy.io import fits
import numpy as np

# original module

### define functions
def combine_frames(input_names, output_name, combine='median'):
# conjunction the input frames
    data_in, header_in = fits.getdata(input_names, view=np.ndarray, header=True)

# combine with mean or median
    if combine == 'mean':
        data_out = np.average(data_in, axis=0)
    else:
        data_out = np.median(data_in, axis=0)

    header_in['R_COMBIN'] = (combine, 'Function used in combining single frames')

# add a comment on the fits header

# output the fits file
    fits.writeto(output_name, data_out, header_in, overwrite=True)

    return

