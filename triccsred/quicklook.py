#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### import libraries
import argparse
import os
import subprocess

from astropy.io import fits
import numpy as np

# original module
from data_red import dark_flat

### define constants
QUICK_DIR = '/home/triccs/work1/quicklook/'
FILTER_DICT = {'PS1-g': 'g', 'PS1-r': 'r', 'PS1-i': 'i', 'PS1-z': 'z',
    'grism_g': 'grism-g', 'grism_r': 'grism-r', 'grism_iz': 'grism-iz',
    'grism-g': 'grism-g', 'grism-r': 'grism-r', 'grism-iz': 'grism-iz'}

### define functions
def quicklook(input_name):
    if not os.path.exists(input_name):
        print(input_name + ' is not exists.\n Quit this process.')
        return

    if os.path.getsize(input_name) == 0:
        print('The file size of ' + input_name + ' 0.\n Quit this process.')
        return

    hdr = fits.getheader(input_name)

# quit this process in some conditions
    filter_grism = hdr['FILTDISP']
    if filter_grism not in FILTER_DICT.keys() or filter_grism == 'dark' or hdr['NAXIS3'] > 100:
        return

    flat_name = '{}lib/flat-{}.fits'.format(QUICK_DIR, FILTER_DICT[filter_grism])

    if not os.path.exists(input_name):
        print(flat_name + 'is not exists.\n Quit this process.')
        return

    data = fits.getdata(input_name, view=np.ndarray)
    data_flat = fits.getdata(flat_name, view=np.ndarray)

    data = dark_flat.div_flat(np.average(dark_flat.do_debias(data), axis=0), data_flat)
#    data = dark_flat.div_flat(dark_flat.combine_average(dark_flat.debias(data)), data_flat)
    output_name = '{}{}-sub-comb.fits'.format(QUICK_DIR, input_name.split('/')[-1].replace('.fits', ''))

    fits.writeto(output_name, data, hdr, overwrite=True)

    if hdr['DETID'] == 1 or hdr['DETID'] == 2:
        subprocess.run('scp {} 192.168.1.88:{}'.format(output_name, QUICK_DIR).split())

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='debias, combine, and flat-fielding TriCCS frame')
    parser.add_argument('input_name', help='input fits file')

    args = parser.parse_args()
    quicklook(args.input_name)

