#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### import libraries
import argparse
import gc
import os
import sys

from astropy.io import fits
import numpy as np

# original module
import data_func

### define constants
tmp_file_name = 'tmp-flat.dat'
step_z_default = 1000

### define functions
def main(input_name, sub_name, output_name, norm_xy=(950, 600), norm_size=(50, 50), step_z=step_z_default):
# check the frame number
    naxis3_list = []
    for tmp_input in input_name:
        with fits.open(tmp_input) as hdul:
            hdr = hdul[0].header
            naxis3_list.append(hdr['NAXIS3'])

# initialize the output array
    size_x, size_y = hdr['NAXIS1'], hdr['NAXIS2']
    naxis3_sum = sum(naxis3_list)
    data_mp = np.memmap(tmp_file_name, dtype='float32', mode='w+', shape=(naxis3_sum, size_y, size_x))

    x1, x2, y1, y2 = norm_xy[0], norm_xy[0] + norm_size[0], norm_xy[1], norm_xy[1] + norm_size[1]

# input dark frame
    data_sub = fits.getdata(sub_name, view=np.ndarray)

# input data, debias, subtract dark, and normalize 
    tmp_frame_z = 0
    for count, tmp_input in enumerate(input_name):
        start_z = 0
        while start_z < naxis3_list[count]:
            end_z = min(start_z + step_z, naxis3_list[count])

            with fits.open(tmp_input, do_not_scale_image_data=True) as hdul:
                bzero = hdul[0].header.pop('BZERO')
                bscale = hdul[0].header.pop('BSCALE')

                for tmp_z in range(start_z, end_z):
                    print('input:', tmp_input, 'processing z =', tmp_z)
                    tmp_data = data_func.debias(hdul[0].data[tmp_z, :, :] * bscale + bzero) - data_sub
#                    tmp_data = hdul[0].data[tmp_z, :, :] * bscale + bzero - data_sub
                    data_mp[tmp_frame_z + tmp_z, :, :] = tmp_data / np.average(tmp_data[y1:y2, x1:x2])
                del hdul[0].data
                gc.collect()

            start_z += step_z

        tmp_frame_z += naxis3_list[count]

# median
    data_out = np.zeros((size_y, size_x))
    for count in range(size_y):
        print('median y =', count)
        data_out[count, :] = np.median(data_mp[:, count, :], axis=0)

    del data_mp
    os.remove(tmp_file_name)

# output file
    print('output')
    fits.writeto(output_name, data_out, hdr, overwrite=True)

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='debias, combine, and subtract TriCCS frame')

    parser.add_argument('input_name', nargs='+', help='input fits file')
    parser.add_argument('sub_name', help='subtract fits file')
    parser.add_argument('output_name', help='output fits file')
    parser.add_argument('--norm_pos_xy', nargs=2, type=int, help='normalize positions in x and y')
    parser.add_argument('--norm_size_xy', nargs=2, type=int, help='normalize size in x and y')
    parser.add_argument('--step_z', type=int, default=step_z_default, help='the frame number in input fits frames')

    args = parser.parse_args()

    kwargs = {}
    if args.norm_pos_xy:
        kwargs['norm_xy'] = args.norm_pos_xy
    if args.norm_size_xy:
        kwargs['norm_size'] = args.norm_size_xy
    if args.step_z:
        kwargs['step_z'] = args.step_z

    main(args.input_name, args.sub_name, args.output_name, **kwargs)

