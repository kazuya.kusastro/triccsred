#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### import libraries
import argparse
import os
import sys

from astropy.io import fits
import numpy as np
from scipy import ndimage

### define constant
shift_table = {'0': (0, 0), '1': (-11, 15), '2': (10, 70)}

### define functions
def main(input_name, dark_name, flat_name, scale=1, shift=(0, 0), rotate=0):
    data, hdr = fits.getdata(input_name, view=np.ndarray, header=True)
    data_dark = fits.getdata(dark_name, view=np.ndarray)
    data_flat = fits.getdata(flat_name, view=np.ndarray)

    data_out = np.divide(data - data_dark, data_flat, out=np.zeros_like(data), where=data_flat!=0)
    data_out = ndimage.shift(data_out, shift, mode='constant')
    data_out = ndimage.rotate(data_out, rotate, mode='constant', reshape=False)

    output_name = input_name[:12] + '-shift.fits'
    fits.writeto(output_name, data_out, hdr, overwrite=True)

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='debias, combine, and subtract TriCCS frame')

    parser.add_argument('input_name', help='input fits file')
    parser.add_argument('dark_name', help='dark fits file')
    parser.add_argument('flat_name', help='flat fits file')
    parser.add_argument('--scale', default=1, help='scale in subtracton')
    parser.add_argument('--shift', nargs=2, type=float, default=(0, 0), help='image shift (y, x)')
    parser.add_argument('--rotate', type=float, default=0, help='image rotation in degree')

    args = parser.parse_args()

    main(args.input_name, args.dark_name, args.flat_name, scale=args.scale, shift=args.shift, rotate=args.rotate)

